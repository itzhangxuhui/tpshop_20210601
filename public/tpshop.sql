/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : tpshop

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2021-12-26 11:47:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tpshop_auth
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_auth`;
CREATE TABLE `tpshop_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auth_name` varchar(255) DEFAULT NULL,
  `auth_controller` varchar(255) DEFAULT NULL,
  `auth_action` varchar(255) DEFAULT NULL,
  `auth_pid` int(11) DEFAULT NULL,
  `auth_path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_auth
-- ----------------------------
INSERT INTO `tpshop_auth` VALUES ('1', '系统管理', null, null, '0', '1');
INSERT INTO `tpshop_auth` VALUES ('2', '管理员管理', 'Manage', 'index', '1', '1,2');
INSERT INTO `tpshop_auth` VALUES ('3', '角色管理', 'Role', 'index', '1', '1,3');
INSERT INTO `tpshop_auth` VALUES ('4', '权限管理', 'Auth', 'index', '1', '1,4');
INSERT INTO `tpshop_auth` VALUES ('5', '商品管理', null, null, '0', '5');
INSERT INTO `tpshop_auth` VALUES ('6', '商品列表', 'Goods', 'index', '5', '5,6');
INSERT INTO `tpshop_auth` VALUES ('7', '商品分类管理', null, null, '0', '7');
INSERT INTO `tpshop_auth` VALUES ('8', '商品分类列表', 'Category', 'index', '7', '7,8');
INSERT INTO `tpshop_auth` VALUES ('9', '会员管理', null, null, '0', '9');
INSERT INTO `tpshop_auth` VALUES ('10', '会员列表', 'Member', 'index', '9', '9,10');
INSERT INTO `tpshop_auth` VALUES ('11', '回收站', 'Member', 'recycle', '9', '9,11');

-- ----------------------------
-- Table structure for tpshop_category
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_category`;
CREATE TABLE `tpshop_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品编号',
  `cate_name` varchar(255) DEFAULT NULL COMMENT '商品名称',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `delete_time` datetime DEFAULT NULL COMMENT '删除时间',
  `pid` int(11) DEFAULT '0' COMMENT '父id',
  `level` int(255) DEFAULT NULL COMMENT '级别',
  `path` varchar(255) DEFAULT NULL COMMENT '路径',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `cate_name` (`cate_name`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tpshop_category
-- ----------------------------
INSERT INTO `tpshop_category` VALUES ('1', '服装', '1637132193', null, '0', '1', '1');
INSERT INTO `tpshop_category` VALUES ('2', '家电', '1637133390', null, '0', '1', '2');
INSERT INTO `tpshop_category` VALUES ('3', '数码产品', '1637136973', '2021-11-29 10:12:32', '0', '1', '3');
INSERT INTO `tpshop_category` VALUES ('4', '男装', '1637137074', null, '1', '2', '1,4');
INSERT INTO `tpshop_category` VALUES ('5', '女装', '1637137156', null, '1', '2', '1,5');
INSERT INTO `tpshop_category` VALUES ('6', '冰箱', '1637138626', null, '2', '2', '2,6');
INSERT INTO `tpshop_category` VALUES ('7', '洗衣机', '1637138634', null, '2', '2', '2,7');
INSERT INTO `tpshop_category` VALUES ('8', '电视', '1637138640', null, '2', '2', '2,8');
INSERT INTO `tpshop_category` VALUES ('9', '数码相机', '1637138845', '2021-11-29 10:10:25', '3', '2', '3,9');
INSERT INTO `tpshop_category` VALUES ('10', '汉服', '1637139006', null, '4', '3', '1,4,10');
INSERT INTO `tpshop_category` VALUES ('11', '超短裙', '1637139611', null, '5', '3', '1,5,11');
INSERT INTO `tpshop_category` VALUES ('12', 'JK制服', '1637139774', null, '5', '3', '1,5,12');
INSERT INTO `tpshop_category` VALUES ('13', '双开门冰箱', '1637217859', null, '6', '3', '2,6,13');
INSERT INTO `tpshop_category` VALUES ('14', '单开门冰箱', '1637217872', null, '6', '3', '2,6,14');
INSERT INTO `tpshop_category` VALUES ('15', '全自动洗衣机', '1637217885', null, '7', '3', '2,7,15');
INSERT INTO `tpshop_category` VALUES ('16', '滚筒洗衣机', '1637217897', null, '7', '3', '2,7,16');
INSERT INTO `tpshop_category` VALUES ('17', '高清电视', '1637217912', null, '8', '3', '2,8,17');
INSERT INTO `tpshop_category` VALUES ('18', '液晶电视', '1637217925', null, '8', '3', '2,8,18');
INSERT INTO `tpshop_category` VALUES ('19', '测试', '1637822041', null, '0', '1', '19');
INSERT INTO `tpshop_category` VALUES ('20', '测试2', '1637822131', null, '19', '2', '19,20');
INSERT INTO `tpshop_category` VALUES ('24', '佳能相机', '1638150505', '2021-11-29 10:08:00', '9', '3', '3,9,24');
INSERT INTO `tpshop_category` VALUES ('23', '西装', '1637908892', '2021-11-29 10:11:23', '4', '3', '1,4,23');
INSERT INTO `tpshop_category` VALUES ('25', '食品', '1638494330', null, '0', '1', '25');

-- ----------------------------
-- Table structure for tpshop_goods
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_goods`;
CREATE TABLE `tpshop_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `goods_name` varchar(255) DEFAULT NULL COMMENT '名称',
  `goods_price` decimal(10,2) DEFAULT NULL COMMENT '价格',
  `goods_num` int(11) DEFAULT NULL COMMENT '库存',
  `sale_num` int(11) DEFAULT '0' COMMENT '销量',
  `brand_id` int(11) DEFAULT NULL COMMENT '品牌id',
  `create_time` datetime DEFAULT NULL COMMENT '上架时间',
  `delete_time` datetime DEFAULT NULL COMMENT '删除时间',
  `status` varchar(255) DEFAULT '0' COMMENT '0上架，1下架',
  `detail` varchar(255) DEFAULT NULL COMMENT '描述',
  `image` varchar(255) DEFAULT NULL COMMENT '图片',
  `cate_id` int(11) DEFAULT NULL COMMENT '分类id',
  `sort_num` int(11) DEFAULT NULL,
  `hit_num` int(11) DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tpshop_goods
-- ----------------------------
INSERT INTO `tpshop_goods` VALUES ('1', '打的费', '1.00', '10', '0', null, '2021-11-25 12:07:41', '2021-11-29 09:13:53', '0', '的说法是否对多福多寿方式方法的说法是否对是否是', 'upload/20211125/1637813099.png', '10', '8', '9');
INSERT INTO `tpshop_goods` VALUES ('2', '打的费2', '2.00', '102', '0', null, '2021-11-25 12:08:15', '2021-11-29 09:13:53', '0', '的说法是否对11', 'upload/20211125/1637813099.png', '10', '44', '15');
INSERT INTO `tpshop_goods` VALUES ('3', '汉服2', '3.00', '100', '0', null, '2021-11-25 12:09:38', null, '0', '这是一件衣服', 'upload/20211130/1638246193.jpg', '10', '657', '331');
INSERT INTO `tpshop_goods` VALUES ('4', '汉服1', '4.00', '100', '0', null, '2021-11-25 12:11:18', null, '0', '这是一件衣服', 'upload/20211130/1638246323.jpg', '10', '1', '29');
INSERT INTO `tpshop_goods` VALUES ('5', '汉服3', '5.00', '100', '0', null, '2021-11-25 12:11:30', null, '0', '11111', 'upload/20211130/1638252800.jpg', '10', '777', '23');
INSERT INTO `tpshop_goods` VALUES ('6', '绘自然 汉服男古风白色魏晋男装侠客书生大码古风男士仙气飘逸霸气古装学生表演班服武侠 【清风', '6.00', '10000', '0', null, '2021-11-26 10:23:08', '2021-11-29 10:11:17', '0', '是放的说', 'upload/20211126/1637910352.jpg', '23', '999', '1');
INSERT INTO `tpshop_goods` VALUES ('7', '裙子啊', '77.00', '88', '0', null, '2021-12-03 10:23:22', null, '0', '', 'upload/20211203/1638498196.jpg', '11', '88', '3');
INSERT INTO `tpshop_goods` VALUES ('8', '洗衣机啊', '77.00', '88', '0', null, '2021-12-03 10:22:40', null, '0', '', 'upload/20211203/1638498147.jpg', '15', '99', '1');
INSERT INTO `tpshop_goods` VALUES ('9', '汉服', '1.00', '100', '0', null, '2021-11-26 17:17:28', null, '0', 'ad辅导费', 'upload/20211126/1637918235.jpg', '10', '55', '17');
INSERT INTO `tpshop_goods` VALUES ('10', 'XXX', '2.00', '55', '0', null, '2021-11-26 17:19:23', null, '0', '是的范德萨范德萨', 'upload/20211130/1638254422.jpg', '11', '36', '5');
INSERT INTO `tpshop_goods` VALUES ('11', '佳能XX相机', '3.00', '100', '0', null, '2021-11-29 10:01:15', '2021-11-29 10:02:01', '0', '这是一个相机', 'upload/20211129/1638151254.jpg', '24', '77', '1');
INSERT INTO `tpshop_goods` VALUES ('12', '冰箱啊', '88.00', '99', '0', null, '2021-12-03 10:22:06', null, '0', '', 'upload/20211203/1638498111.jpg', '13', '88', '1');
INSERT INTO `tpshop_goods` VALUES ('13', '唐努乌 电竞少女jk制服学生百褶裙格裙加绒衬衫校服日系正统半身裙学院风短伞状', '4.00', '10', '0', null, '2021-11-30 14:26:47', null, '0', '是放的说法都是', 'upload/20211130/1638254369.jpg', '12', '7', '3');
INSERT INTO `tpshop_goods` VALUES ('14', '上流汇jk制服少女格裙子全套装秋冬季不规则打底半身裙百褶裙连衣裙学生装校服', '5.00', '8', '0', null, '2021-11-30 14:25:01', null, '0', 'dsfdsfdsf', 'upload/20211130/1638253499.jpg', '12', '25', '15');
INSERT INTO `tpshop_goods` VALUES ('15', 'hahaha', '1234.00', '100', '0', null, null, null, '0', null, null, '12', null, '1');
INSERT INTO `tpshop_goods` VALUES ('16', '沙发斯蒂芬', '11.00', '11', '0', null, null, null, '0', '11111', 'goods/20211223\\6a42aed669446b33d846c50d18569100.jpg', '10', null, '1');
INSERT INTO `tpshop_goods` VALUES ('17', '的是放的说法都是', '111.00', '11', '0', null, null, null, '0', '1111', 'goods/20211223\\5818ef06c9a064a56c08eb6db709e16a.jpg', '23', null, '1');
INSERT INTO `tpshop_goods` VALUES ('18', '衣服', '100.00', '50', '0', null, null, null, '0', '对方的身份', 'goods/20211224\\70ac6520f1c0eddec2c42ad4c59eb785.jpg', '11', null, '1');
INSERT INTO `tpshop_goods` VALUES ('19', '啊啊啊', '11.00', '11', '0', null, null, null, '0', '对方答复', 'goods/20211224\\22bd67b2299eb5122b0b9b103f0ef268.jpg', '11', null, '1');

-- ----------------------------
-- Table structure for tpshop_goods_pic
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_goods_pic`;
CREATE TABLE `tpshop_goods_pic` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_goods_pic
-- ----------------------------
INSERT INTO `tpshop_goods_pic` VALUES ('1');

-- ----------------------------
-- Table structure for tpshop_manage
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_manage`;
CREATE TABLE `tpshop_manage` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `manage_name` varchar(255) DEFAULT NULL COMMENT '管理员名字',
  `manage_pass` varchar(255) DEFAULT NULL COMMENT '管理员密码',
  `create_time` int(11) DEFAULT NULL COMMENT '添加时间',
  `delete_time` int(11) DEFAULT NULL COMMENT '删除时间',
  `is_forbidden` tinyint(255) DEFAULT '0' COMMENT '禁用，0不禁用，1禁用',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `manage` (`manage_name`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tpshop_manage
-- ----------------------------
INSERT INTO `tpshop_manage` VALUES ('1', 'admin', '1720d2a66c2f107e018b02c3095e8bdf', null, null, '0');
INSERT INTO `tpshop_manage` VALUES ('2', '张三', 'fce86a4ca92f2b135d81b8149d8f435f', '1640487447', null, '0');
INSERT INTO `tpshop_manage` VALUES ('3', '李四', '16d741a8dba33dcffe184d42ec856f3f', '1640487463', null, '0');
INSERT INTO `tpshop_manage` VALUES ('4', 'chenbin', '941b1f45b1e08f963804b41101861b2c', '1640487481', null, '0');
INSERT INTO `tpshop_manage` VALUES ('5', '苏新龙', 'a7ff6b9c90a7e1f8c4331826992e9f87', '1640489266', null, '0');

-- ----------------------------
-- Table structure for tpshop_manage_role
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_manage_role`;
CREATE TABLE `tpshop_manage_role` (
  `mid` int(11) DEFAULT NULL,
  `rid` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_manage_role
-- ----------------------------
INSERT INTO `tpshop_manage_role` VALUES ('2', '1');
INSERT INTO `tpshop_manage_role` VALUES ('3', '2');
INSERT INTO `tpshop_manage_role` VALUES ('4', '3');
INSERT INTO `tpshop_manage_role` VALUES ('5', '4');

-- ----------------------------
-- Table structure for tpshop_member
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_member`;
CREATE TABLE `tpshop_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `delete_time` int(11) DEFAULT NULL,
  `login_num` int(11) DEFAULT '1',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_member
-- ----------------------------
INSERT INTO `tpshop_member` VALUES ('6', 'xiaoming', '12323', null, '1', null, null, '1');
INSERT INTO `tpshop_member` VALUES ('7', 'daming', '456456', null, '2', null, null, '1');
INSERT INTO `tpshop_member` VALUES ('13', '小白', '999999', null, '1', null, null, '1');
INSERT INTO `tpshop_member` VALUES ('9', 'aaa', 'cccccc', '1639965068', '2', null, null, '1');
INSERT INTO `tpshop_member` VALUES ('11', '小王', null, null, '1', null, null, '2');
INSERT INTO `tpshop_member` VALUES ('12', '小王aa', null, null, '1', null, null, '2');
INSERT INTO `tpshop_member` VALUES ('15', '小白白', '8888888', null, '1', null, null, '2');
INSERT INTO `tpshop_member` VALUES ('16', '老白', '000000', '1640144091', '1', null, null, '2');
INSERT INTO `tpshop_member` VALUES ('17', 'xiaomi', '000000', '1640143518', '1', null, null, '2');
INSERT INTO `tpshop_member` VALUES ('18', 'aaaaaaa', '3b97bd5aed74ad1eb733692f4676a073', '1640143362', '1', null, null, '1');
INSERT INTO `tpshop_member` VALUES ('19', 'sunwukong', '84011b9daf612324d15b4ac38d37a7cc', '1640136428', '1', null, null, '2');
INSERT INTO `tpshop_member` VALUES ('20', 'zhubajie', '5d9b8a37d718f5a8506be9fdd84f24aa', '1640136285', '1', null, null, '1');
INSERT INTO `tpshop_member` VALUES ('21', 'lalala', null, '1640143077', '1', null, null, '2');
INSERT INTO `tpshop_member` VALUES ('22', 'kjkkjdkfdf12', null, '1640143024', '1', null, null, '1');
INSERT INTO `tpshop_member` VALUES ('23', 'thinkphp', null, null, '1', '1640153637', '1640153637', '1');
INSERT INTO `tpshop_member` VALUES ('24', 'helloaxiaoming', null, null, '1', '1640153722', '1640153722', '2');
INSERT INTO `tpshop_member` VALUES ('25', 'thinkphp123', null, null, '1', '1640154082', '1640154082', '1');
INSERT INTO `tpshop_member` VALUES ('26', 'thinkphp123456', null, null, '1', '1640154120', '1640154120', '1');
INSERT INTO `tpshop_member` VALUES ('27', '888888', null, null, '1', null, null, '1');
INSERT INTO `tpshop_member` VALUES ('28', '\'dfdfdfdfdfdsfdfsdf\'', 'b2c3d71fa1be32ad2153b9a5b6b58250', null, '1', null, null, null);

-- ----------------------------
-- Table structure for tpshop_role
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_role`;
CREATE TABLE `tpshop_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) DEFAULT NULL,
  `role_detail` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_role
-- ----------------------------
INSERT INTO `tpshop_role` VALUES ('1', '技术部', '我是技术部');
INSERT INTO `tpshop_role` VALUES ('2', '市场部', '我是市场部');
INSERT INTO `tpshop_role` VALUES ('3', '人事部', '我是人事部的');
INSERT INTO `tpshop_role` VALUES ('4', '添加管理员的角色', '');

-- ----------------------------
-- Table structure for tpshop_role_auth
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_role_auth`;
CREATE TABLE `tpshop_role_auth` (
  `rid` int(11) DEFAULT NULL,
  `aid` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_role_auth
-- ----------------------------
INSERT INTO `tpshop_role_auth` VALUES ('1', '1');
INSERT INTO `tpshop_role_auth` VALUES ('1', '2');
INSERT INTO `tpshop_role_auth` VALUES ('1', '3');
INSERT INTO `tpshop_role_auth` VALUES ('1', '4');
INSERT INTO `tpshop_role_auth` VALUES ('1', '5');
INSERT INTO `tpshop_role_auth` VALUES ('1', '6');
INSERT INTO `tpshop_role_auth` VALUES ('1', '7');
INSERT INTO `tpshop_role_auth` VALUES ('1', '8');
INSERT INTO `tpshop_role_auth` VALUES ('1', '9');
INSERT INTO `tpshop_role_auth` VALUES ('1', '10');
INSERT INTO `tpshop_role_auth` VALUES ('1', '11');
INSERT INTO `tpshop_role_auth` VALUES ('2', '5');
INSERT INTO `tpshop_role_auth` VALUES ('2', '6');
INSERT INTO `tpshop_role_auth` VALUES ('2', '7');
INSERT INTO `tpshop_role_auth` VALUES ('2', '8');
INSERT INTO `tpshop_role_auth` VALUES ('3', '9');
INSERT INTO `tpshop_role_auth` VALUES ('3', '10');
INSERT INTO `tpshop_role_auth` VALUES ('3', '11');
INSERT INTO `tpshop_role_auth` VALUES ('4', '1');
INSERT INTO `tpshop_role_auth` VALUES ('4', '2');
