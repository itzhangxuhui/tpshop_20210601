<?php
declare (strict_types = 1);

namespace app\index\controller;

use think\facade\Session;
use think\facade\Db;
use think\Request;


class Member
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $mid = Session::get("mid");
        $row = Db::name("member")->find($mid);
        return view("",["row"=>$row]);
    }


}
