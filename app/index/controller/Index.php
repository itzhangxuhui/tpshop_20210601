<?php
namespace app\index\controller;

use app\BaseController;
use think\facade\Db;
use think\facade\Session;
use shop\SMTP;
use shop\PHPMailer;


// 导入对应产品模块的client
use TencentCloud\Sms\V20210111\SmsClient;
// 导入要请求接口对应的Request类
use TencentCloud\Sms\V20210111\Models\SendSmsRequest;
use TencentCloud\Common\Exception\TencentCloudSDKException;
use TencentCloud\Common\Credential;
// 导入可选配置类
use TencentCloud\Common\Profile\ClientProfile;
use TencentCloud\Common\Profile\HttpProfile;



class Index extends BaseController
{
    // 首页
    public function index()
    {
        //向用户添加处理接口发起一次请求
   /*     $url = "http://www.zhangxuhui.com/admin/Member/save?username='dfdfdfdfdfdsfdfsdf'&password='87989787878787'";
        dump(file_get_contents($url));*/

        return view();
    }

    // 登录
    public function login(){
        $urlencode = urlencode("http://www.zhangxuhui.com/index/Index/callback");
        $url = "https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id=101938823&redirect_uri={$urlencode}&state=shop";
        $data = compact("url");
        return view("",$data);
    }

    // 登录处理
    public function loginAction(){
        $account = request()->param("account"); // 用户的账号
        $password = md5(md5(request()->param("password"))); // 用户的密码
        // 账号可以用，用户名，邮箱，手机号 都可以登录，该如何实现？
        $reg_phone = '/^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/';
        $reg_email = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/';
        $where["password"] = $password;
        $where['is_active'] = 1;
        if(preg_match($reg_phone,$account)){
            // 用手机号来登录
            $where['phone'] = $account;
        }elseif (preg_match($reg_email,$account)){
            // 用邮箱来登录
            $where['email'] = $account;
        }else{
            // 用户名来登录
            $where['username'] = $account;
        }
        if($row = Db::name("member")->where($where)->find()){
            // 用户名，密码，都正确，已经被激活了
            $data['code'] = 0;
            // 存session
            Session::set("member_flag",1);
            Session::set("mid",$row['id']);
            Session::set("username",$row['username']);
            // 查询上次的登录时间
//            $last_login_time = Db::name("member")->where(["id"=>$row['id']])->value("login_time");
            Session::set("last_login_time",$row['login_time']); // 把上次登录时间取出来，存session
            // 每次登录成功后，修改登录时间
            Db::name("member")->where(["id"=>$row['id']])->update(["login_time"=>date("Y-m-d H:i:s")]);
        }else{
            $data['code'] = 1;
        }
        return json($data);

    }
    // 回调地址
    public function callback($code){
        // 1.获取code
        // 2.发起请求 获取access_token
        $urlencode = urlencode("http://www.zhangxuhui.com/index/Index/callback");
        $url = "https://graph.qq.com/oauth2.0/token?grant_type=authorization_code&client_id=101938823&client_secret=9fe22ece9566ea6998d2dcc9a53024dd&code={$code}&redirect_uri={$urlencode}&fmt=json";
        $json = file_get_contents($url);
        $obj = json_decode($json);
        $access_token = $obj->access_token;
        // 3.发起请求 获取openid
        $url = "https://graph.qq.com/oauth2.0/me?access_token={$access_token}&fmt=json";
        $obj = json_decode(file_get_contents($url));
        $openid = $obj->openid;
        // 4.获取用户的信息
        $url = "https://graph.qq.com/user/get_user_info?access_token={$access_token}&oauth_consumer_key=101938823&openid={$openid}";
        $obj = json_decode(file_get_contents($url));
        // openid
        // 1.根据openid 查询这条记录是否已经存在？如果不存在，则写入，如果存在，则不用写
        $row = Db::name("qq")->where(["openid"=>$openid])->find();
        if($row){
            // 如果查到了，意味着此QQ已经登录过了
            // 查询是否已经绑定
            if($row['mid']){
                // 已经绑定了 用户信息存入session
                $userinfo = Db::name("member")->where(["id"=>$row['mid']])->find();
                // 绑定成功了
                Session::set("member_flag",1);
                Session::set("mid",$row['mid']);
                Session::set("username",$userinfo['username']);
                // 跳转到首页
                return redirect((string)url('index'));
            }else{
                // 还未绑定
                // 跳转到绑定账号的页面
                // 把openid 存入session
                Session::set("openid",$openid);
                Db::name("qq")->where("openid",$openid)->inc("login_num")->update();
                return redirect((string)url('bindAccount'));
            }
        }else{
            // 说明初次登录
            // 1.qq_member表写入数据,id,openid,nickname,gender,login_num,mid
            $data["nickname"] = $obj->nickname;
            $data["gender"] = $obj->gender;
            $data["openid"] = $openid;
            $data["login_num"] = 1;
            $id = Db::name("qq")->insertGetId($data); // 假如写入成功
            // 跳转到绑定账号的页面
            // 把openid 存入session
            Session::set("openid",$openid);
            return redirect((string)url('bindAccount'));
        }


        // 2.写session

        // 3. 跳转到首页

    }
    // 绑定账号
    public function bindAccount(){
        return view();
    }
    // 绑定处理
    public function bindAccountAction(){
        // 绑定处理
        $username = request()->param("username");
        $password = md5($username.request()->param("password"));
        if($row = Db::name("member")->where(["username"=>$username,"password"=>$password,"is_active"=>1])->find()){
            // 绑定的账号，密码没问题
            // 所谓的绑定就是把用户的id写入到qq表的mid
            $openid = Session::get("openid");
            if(Db::name("qq")->where(["openid"=>$openid])->update(["mid"=>$row['id']])){
                // 绑定成功了
                Session::set("member_flag",1);
                Session::set("mid",$row['id']);
                Session::set("username",$username);
                // 跳转到首页
                $data['code'] = 0;
            }else{
                // 绑定失败了
                $data['code'] = 1;
            }
            return json($data);

//            $data['code'] = 0;
//            // 存session

        }else{
            $data['code'] = 1;
        }
        return json($data);
    }

    // 注册
    public function register(){
        return view();
    }

    // 邮箱注册处理
    public function regActEmail(){
        $username = $this->request->param("username");
        if($this->checkUserIsExists("username",$username)){
            // 此用户名不可用
            return json(["code"=>1]);
        }
        $email = $this->request->param("email");
        if($this->checkUserIsExists("email",$email)){
            // 此邮箱不可用
            return json(["code"=>2]);
        }
        // 注册
        $password = md5(md5($this->request->param("password")));
        $bool = Db::name("member")
            ->insertGetId(["username"=>$username,"password"=>$password,"email"=>$email,"create_time"=>time()]);
        if($bool){
            // 注册成功 开始发送邮件
            if(!$this->sendEmail($email)){
                // 删除此用户，提示用户重新发送
                Db::name("member")->delete($bool);
                return json(["code"=>4]);
            }
            return json(["code"=>0]); // 注册成功了
        }else{
            // 注册失败
            return json(["code"=>3]);
        }


    }


    // 手机号注册处理
    public function regActPhone(){
        $yzm = request()->param("yzm"); // 用户传递的
        $smscode = Session::get("smscode");

        if($yzm != $smscode){
            return json(["code"=>1]); // 1.验证码不一致
        }
        $phone = request()->param("phone"); // 用户传递过来的
        $smsphone = Session::get("smsphone"); // 发短信时候的手机号
        if($phone != $smsphone){
            return json(["code"=>2]); // 2.手机号不正确
        }
        // 无需在判断当前的手机号是否已经注册过，因为此手机号一定没有注册过
        // 校验用户名
        $username = request()->param("username");
        if(Db::name("member")->where(["username"=>$username])->find()){
            return json(["code"=>3]); // 3.用户名已存在
        }
        // 如果不存在，则写数据库
        $password = md5(md5(request()->param("password")));
        $data["username"] = $username;
        $data['password'] = $password;
        $data['phone'] = $phone;
        $data['create_time'] = time();
        $data['is_active'] = 1;

        if(Db::name("member")->insertGetId($data)){
            return json(["code"=>0]); // 4.手机号注册成功
        }else{
            return json(["code"=>4]); // 5.手机号注册失败
        }
    }

    // 校验是否已经存在此用户
    public function checkUserIsExists($user,$val){
        return Db::name("member")->where([$user=>$val])->find();
    }

    // 发送邮件的
    public function sendEmail($address)
    {
        $mail = new PHPMailer();
        $mail->IsSMTP(); // 启用SMTP
        $mail->Host = "smtp.yeah.net"; //SMTP服务器 以163邮箱为例子
        $mail->Port = 25;  //邮件发送端口
        $mail->SMTPAuth = true;  //启用SMTP认证

        $mail->CharSet = "UTF-8"; //字符集
        $mail->Encoding = "base64"; //编码方式

        $mail->Username = "phpmaster@yeah.net";  //你的邮箱
        $mail->Password = "UYHKPQFPIUMEGRIG";  //网易邮箱客户端授权码

        $mail->From = "phpmaster@yeah.net";  //发件人地址（也就是你的邮箱）
        $mail->FromName = "张某某";  //发件人姓名

//        $address = "137647337@qq.com";//收件人email
        $mail->AddAddress($address, "亲");//添加收件人（地址，昵称）
//附件
        /*$mail->AddAttachment("logo.jpg");
        $mail->AddAttachment("1.zip");
        $mail->AddAttachment('1.xls','我的附件.xls'); // 添加附件,并指定名称*/
        $mail->IsHTML(true); //支持html格式内容
//$mail->AddEmbeddedImage("logo.jpg", "my-attach", "logo.jpg"); //设置邮件中的图片
        $mail->Subject = "你好"; //邮件标题
        $code = base64_encode($address);
        $mail->Body = "欢迎您注册成功，请点击激活,<a href='http://www.tp6.com/index/Index/active/code/{$code}'>激活</a>"; //邮件主体内容
        return $mail->send();

    }
    // 激活
    public function active($code){
        // 判断是否过期，假定3分钟之内可以激活，超过3分钟 就算激活失败
        // 1.注册时间  2.当前时间
        $code = base64_decode($code);
        // 1.判断是否已经激活成功了
        $row = Db::name("member")->where(["email"=>$code])->find();
        if($row['is_active']){
            // 已经激活成功了
            return redirect((string)url('activeSuccess'));
        }
        // 2.没有被激活现在开始激活
        // 1.判断有没有过期
        if(time()-$row['create_time'] > 1800){
            // 过期了 直接跳转一个过期的页面
            return redirect((string)url('overtime'));
        }else{
            // 激活
            if(Db::name("member")->where(["email"=>$code])->update(["is_active"=>1])){
                // 激活成功了
                return redirect((string)url('activeSuccess'));
            }else{
                // 跳转一个激活失败的页面
                return redirect((string)url('activeFail'));
            }
        }

    }
    // 过期页面
    public function overtime(){
        return view();
    }
    // 激活成功的页面
    public function activeSuccess(){
        return view();
    }
    // 激活失败了
    public function activeFail(){
        return view();
    }

    // 退出
    public function loginOut(){
        // 清除session
        Session::delete('member_flag');
        // 跳转页面
        return redirect((string)url('login'));
    }


    // 发送短信
    public function sendSms($phone){

        // 查询此手机号是否已经注册过
        if(Db::name("member")->where(["phone"=>$phone])->find()){
            return json(["code"=>1]);
        }

        try {
            /* 必要步骤：
             * 实例化一个认证对象，入参需要传入腾讯云账户密钥对secretId，secretKey。
             * 这里采用的是从环境变量读取的方式，需要在环境变量中先设置这两个值。
             * 你也可以直接在代码中写死密钥对，但是小心不要将代码复制、上传或者分享给他人，
             * 以免泄露密钥对危及你的财产安全。
             * CAM密匙查询: https://console.cloud.tencent.com/cam/capi*/
            /********************************************1.secretId,secretKey***********************************************/
            $cred = new Credential("AKID52nKekbFj03rQmupkhV6TcYGkzzkGXZN", "e5To4DAtQildTIzSikaCakgNq55qNfiW");
            //$cred = new Credential(getenv("TENCENTCLOUD_SECRET_ID"), getenv("TENCENTCLOUD_SECRET_KEY"));

            // 实例化一个http选项，可选的，没有特殊需求可以跳过
            $httpProfile = new HttpProfile();
            // 配置代理
            // $httpProfile->setProxy("https://ip:port");
            $httpProfile->setReqMethod("GET");  // post请求(默认为post请求)
            $httpProfile->setReqTimeout(30);    // 请求超时时间，单位为秒(默认60秒)
            $httpProfile->setEndpoint("sms.tencentcloudapi.com");  // 指定接入地域域名(默认就近接入)

            // 实例化一个client选项，可选的，没有特殊需求可以跳过
            $clientProfile = new ClientProfile();
            $clientProfile->setSignMethod("TC3-HMAC-SHA256");  // 指定签名算法(默认为HmacSHA256)
            $clientProfile->setHttpProfile($httpProfile);

            // 实例化要请求产品(以sms为例)的client对象,clientProfile是可选的
            // 第二个参数是地域信息，可以直接填写字符串 ap-guangzhou，或者引用预设的常量
            $client = new SmsClient($cred, "ap-guangzhou", $clientProfile);

            // 实例化一个 sms 发送短信请求对象,每个接口都会对应一个request对象。
            $req = new SendSmsRequest();

            /* 填充请求参数,这里request对象的成员变量即对应接口的入参
             * 你可以通过官网接口文档或跳转到request对象的定义处查看请求参数的定义
             * 基本类型的设置:
             * 帮助链接：
             * 短信控制台: https://console.cloud.tencent.com/smsv2
             * sms helper: https://cloud.tencent.com/document/product/382/3773 */

            /************************ 2.短信应用ID: 短信SdkAppId在 [短信控制台] 添加应用后生成的实际SdkAppId，示例如1400006666 */
            $req->SmsSdkAppId = "1400616870";
            /**************************3.短信签名内容: 使用 UTF-8 编码，必须填写已审核通过的签名，签名信息可登录 [短信控制台] 查看 */
            $req->SignName = "芒果鱼个人网";
            /* 短信码号扩展号: 默认未开通，如需开通请联系 [sms helper] */
            $req->ExtendCode = "";
            /* 下发手机号码，采用 E.164 标准，+[国家或地区码][手机号]
             * 示例如：+8613711112222， 其中前面有一个+号 ，86为国家码，13711112222为手机号，最多不要超过200个手机号*/
            /**************************4.手机号*****************************/
            $req->PhoneNumberSet = array($phone);
            /* 国际/港澳台短信 SenderId: 国内短信填空，默认未开通，如需开通请联系 [sms helper] */
            $req->SenderId = "";
            /* 用户的 session 内容: 可以携带用户侧 ID 等上下文信息，server 会原样返回 */
            $req->SessionContext = "xxx";
            /****************************5.模板 ID: 必须填写已审核通过的模板 ID。模板ID可登录 [短信控制台] 查看 */
            $req->TemplateId = "1262642";
            /****************************6.模板变量 模板参数: 若无模板参数，则设置为空*/
            $code = mt_rand(1000,9999);
            // 存session
            Session::set("smscode",$code);
            Session::set("smsphone",$phone); // 保存当前的发短信的手机号
            $req->TemplateParamSet = array($code);

            // 通过client对象调用SendSms方法发起请求。注意请求方法名与请求对象是对应的
            // 返回的resp是一个SendSmsResponse类的实例，与请求对象对应
            $resp = $client->SendSms($req);// 如果发送成功，返回值是什么？
            // 如果发送失败，返回值是什么
            // 输出json格式的字符串回包
//            print_r($resp->toJsonString());
            // 也可以取出单个值。
            // 您可以通过官网接口文档或跳转到response对象的定义处查看返回字段的定义
//            print_r($resp->TotalCount);
            return json(["code"=>0]);

        }
        catch(TencentCloudSDKException $e) {
            echo $e;
        }
    }

}
