<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\facade\Db;
use think\Request;

class Manage
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    // 管理员列表
    public function index()
    {
        // 获取所有管理员信息
        $manages = Db::name("manage")
            ->alias("m")
            ->field("m.id,m.manage_name,m.create_time,r.role_name")
            ->join("manage_role mr","m.id=mr.mid")
            ->join("role r","r.id=mr.rid")
            ->select();
        $data = compact("manages");
        return view("",$data);
    }
    // 管理员添加
    public function add(){
        // 获取所有的角色
        $roles = Db::name("role")->select();
        $data = compact("roles");
        return view("",$data);
    }

    // 管理员添加处理
    public function addAction(){
        $manage_name = request()->param("manage_name");
        $manage_pass = request()->param("manage_pass");
        $create_time = time();
        $role_id = request()->param("role_id");
        if(Db::name("manage")->where(["manage_name"=>$manage_name])->find()){
            $da['code'] = 1; // 查到了
        }else{
            // 查不到
           $manage_pass = mima($manage_name,$manage_pass);
           // 管理员表入库
            $id = Db::name("manage")->insertGetId(["manage_name"=>$manage_name,"manage_pass"=>$manage_pass,"create_time"=>$create_time]);
            if($id){
                // 写入成功 写入管理员角色表
                if(Db::name("manage_role")->insertGetId(["mid"=>$id,"rid"=>$role_id])){
                    $da['code'] = 0; // 成功了
                }else{
                    // 写入失败了
                    while (!($res = Db::name("manage")->delete($id))){}
                    $da['code'] = 2; // 查到了
                }

            }else{
                // 写入失败
                $da['code'] = 2; // 查到了
            }
        }

        return json($da);
    }


}
