<?php
declare (strict_types = 1);

namespace app\admin\controller;

use app\admin\model\GoodsPicModel;
use app\admin\model\MemberModel;
use think\facade\Cache;
use think\facade\Db;
use think\Model;
use think\Request;
//use app\admin\model\Member as M;
//use app\admin\model\Member as M;

class Member
{

    public function test(){
        Cache::set("name","thinkh");
//        $member = new MemberModel();
//        $obj = MemberModel::find(24);
//        echo $obj->create_time;
//        echo "<br>";
//        echo $obj->update_time;
//        $member->username = '888888';
////
//        $member->save();
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $rows = Db::name("member")->whereNull("delete_time")->order("id desc")->select();
        $data = compact("rows");
        return view("",$data);
    }

    public function recycle(){
        return view();
    }
    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        return view();
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
//        $user->allowField(['name','email'])->save($request->all());
       $username = $request->param("username");
       $password = md5($username.$request->param("password"));
       $data = compact("username","password");
//        $member = new MemberModel();
//        $member->save($data);
       if(Db::name("member")->insertGetId($data)){
           $da['code'] = 0;
       }else{
           $da['code'] = 1;
       }
       return json($da);
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        // 根据id 查询一条记录
        $row = Db::name("member")->find($id);
        $data = compact('row');
        return view("",$data);
    }
    // 检查用户名
    public function checkUsername($uval){
        if(Db::name("member")->where(["username"=>$uval])->find()){
            $data['code'] = 1;
        }else{
            $data['code'] = 0;
        }
        return json($data);

    }
    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request)
    {
        $user = MemberModel::find($request->param("id"));
//        $user->username     = $request->param("username");

        if($user->save(["username"=>$request->param("username")])){
            $data['code'] = 0;
        }else{
            $data['code'] = 1;
        }
        return json($data);
      /*  if(Db::name("member")->save($request->only(["id","username"]))){
            // 修改成功
            $data['code'] = 0;
        }else{
            // 修改失败
            $data['code'] = 1;
        }
        return json($data);*/
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
//        $obj = MemberModel::where("id","=",$id)->delete();

       /* $obj = MemberModel::find($id);
        $obj->delete();*/
//        MemberModel::destroy($id);

        MemberModel::destroy(function ($query) use ($id){
            $query->where('id','=',$id);
//            dd($id);
        } );
     /*   if(Db::name("member")->where(["id"=>$id])->useSoftDelete("delete_time",time())->delete()){
            $data['code'] = 0;
        }else{
            $data['code'] = 1;
        }
        return json($data);*/
    }
}
