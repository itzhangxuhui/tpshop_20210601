<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\facade\Db;
use think\Request;

class Role
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $roles = Db::name("role")->select()->toArray();
        foreach ($roles as &$v){
            $rid = $v['id']; // 获取当前的角色id
            // 根据角色id去查询所有的权限id
            $aids = Db::name("role_auth")->where(["rid"=>$rid])->column("aid");
            // 根据权限id 查询所有的权限名称
            $auth_names = Db::name("auth")->whereIn("id",$aids)->column("auth_name");
            $str = "";
            foreach ($auth_names as $auth_name){
                $str .= $auth_name.",";
            }
            $v["auth"] = $str;
        }
//        dd($roles);
        $data = compact("roles");

        return view("",$data);
    }

    // 角色添加
    public function add(){
        $levels = getTree(Db::name("auth")->column('*','id'));
        $data = compact("levels");
        return view("",$data);
    }

    // 角色添加处理
    public function addAction(){
        $data = request()->only(["role_name","role_detail"]);
        $aids = request()->param("aids");
        // 1.角色表写入数据
        $rid = Db::name("role")->insertGetId($data);
        // 2.角色权限表写入数据
        foreach ($aids as $aid){
            Db::name("role_auth")->insertGetId(["rid"=>$rid,"aid"=>$aid]);
        }
        return json(["code"=>0]);
    }
    // 角色编辑
    public function edit($id){
        // 根据角色id查询角色信息
        $role = Db::name("role")->find($id);
        $aids = Db::name("role_auth")->where(["rid"=>$id])->column("aid");
        $levels = getTree(Db::name("auth")->column('*','id'));
        $data = compact("levels","role","aids");
        return view("",$data);
    }

    // 角色编辑处理
    public function editAction($id){
        // 删除role_auth 表对应数据
        Db::name("role_auth")->where(["rid"=>$id])->delete();
        $data = request()->only(["role_name","role_detail"]);
        $aids = request()->param("aids");
        // 1.角色表修改数据
        Db::name("role")->where(["id"=>$id])->update($data);
        // 2.角色权限表写入数据
        foreach ($aids as $aid){
            Db::name("role_auth")->insertGetId(["rid"=>$id,"aid"=>$aid]);
        }
        return json(["code"=>0]);

    }


}
