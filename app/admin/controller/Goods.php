<?php
declare (strict_types = 1);

namespace app\admin\controller;

use shop\Page;
use think\facade\Cookie;
use think\facade\Db;
use think\facade\Session;
use think\facade\Request;
class Goods
{
    // tp 提供的分页类实现分页
    public function index(){
        // 提交 这里 获取数据
  /*      $row = request()->only(["goods_name","start","end"]);
        dump($row);*/
        $goods_name = request()->param("goods_name");
        $start = request()->param("start");
        $end = request()->param("end");
        $where = [];
        if(!is_null($goods_name)){
            // 如果有搜索条件，那么加入搜索条件
            $where[] = ["goods_name","like","%$goods_name%"];
        }
        if($start != ""){
            // 如果有搜索条件，那么加入搜索条件
            $where[] = ["g.create_time",">=",$start];
        }
        if($end != ""){
            // 如果有搜索条件，那么加入搜索条件
            $where[] = ["g.create_time","<=",$end];
        }
        $rows = Db::name("goods")
            ->alias("g")
            ->field("g.*,c.cate_name")
            ->join("category c","c.id=g.cate_id")
            ->where($where)
            ->order('id desc')
            ->paginate([
                'list_rows'=> 3,
                'query' => ["goods_name"=>$goods_name,"start"=>$start,"end"=>$end],
            ]);
//        echo $rows->total(); // 获取搜索到的总记录数
        $data = compact("rows");
        return view("",$data);
    }
    public function create(){
        // 获取一级分类
        $level1 = Db::name("category")->where("pid",0)->order("id")->select();
        $data = compact("level1");
        return view("",$data);
    }

    // 图片上传
    public function uploadImg(){       // 上传代码
        $file = request()->file('file');
        // 上传到本地服务器
        $savename = \think\facade\Filesystem::disk('public')->putFile( 'goods', $file);
        // 上传成功了 缩略图
        $image = \think\Image::open('./upload/'.$savename);
        $image->thumb(300, 300)->save('./upload/goods/thumb/thumb.png');
        $data['image'] = $savename;
        $data['code'] = 0;

        return json($data);

    }

    // 获取二级分类
    public function getSon($id){
        $data['sons'] = Db::name("category")->where(["pid"=>$id])->select();
        $data['sql'] = Db::getLastSql();
        return json($data);
    }

    // 添加商品处理
    public function addAction(){
//        dd(request()->all());
        $data = request()->only(["goods_name","goods_price","goods_num","cate_id","detail","image"]);
        $data['create_time'] = date("Y-m-d H:i:s");
         if(Db::name("goods")->strict(false)->insertGetId($data)){
             $da['code'] = 0;
         }else{
             $da['code'] = 1;
         }
         return json($da);
    }


}
