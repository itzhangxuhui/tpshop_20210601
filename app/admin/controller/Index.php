<?php
namespace app\admin\controller;

use app\BaseController;
use think\facade\Session;

//use app\middleware\Check;
use think\facade\Db;

class Index extends BaseController
{
    public function index()
    {
        $mid = Session::get("admin_id");
        if($mid == 1){
            $auths = Db::name("auth")->column("*","id");
        }else{
            // 1.根据当前登录的id 查询 当前管理员的角色id
            $rid = Db::name("manage_role")->where(["mid"=>$mid])->value("rid");
            // 2.根据当前管理员的角色id 查询当前管理的权限
            $aids = Db::name("role_auth")->where(["rid"=>$rid])->column("aid");
            // 3.根据权限id 查询所属权限
            $auths = Db::name("auth")->whereIn("id",$aids)->column("*","id");
        }
        $auths = getTree($auths);
        $data = compact("auths");
        return view("",$data);

    }

    // welcome
    public function welcome(){
        return view();
    }
    public function login(){
        return view();
    }
    public function loginAction(){
        // 用户输入的验证码
        $captcha = request()->param("yzm");

        if(!captcha_check($captcha)){
            // 验证失败
            $data['code'] = 1;
        }else{
            // 验证用户名和密码
            $manage_name = $this->request->param("username");
            $manage_pass = strrev(md5(md5($manage_name.$this->request->param("password"))));
            $row = Db::name("manage")->where(["manage_name"=>$manage_name,"manage_pass"=>$manage_pass])->find();
            if($row){
                Session::set("admin_flag",1);
                Session::set("admin_id",$row['id']);
                Session::set("manage_name",$row['manage_name']);
                $data['code'] = 0;
            }else{
                $data['code'] = 2;
            }


        }
        return json($data);
    }


    // 退出
    public function loginOut(){
        // 清除session
        Session::delete('admin_flag');

        // 跳转页面
        return redirect((string)url('login'));
    }

}
