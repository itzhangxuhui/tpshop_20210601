<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\Request;

class Test
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        // 图像处理
        $image = \think\Image::open('./1.jpg');
        // 返回图片的宽度
//        $width = $image->width();
//// 返回图片的高度
//        $height = $image->height();
//// 返回图片的类型
//        $type = $image->type();
//// 返回图片的mime类型
//        $mime = $image->mime();
//// 返回图片的尺寸数组 0 图片宽度 1 图片高度
//        $size = $image->size();
//
//        dump($width,$height,$type,$mime,$size);

        //将图片裁剪为300x300并保存为crop.png
//        $image->crop(300, 300,300,10)->save('./crop.jpg');
//        $image->thumb(300, 300)->save('./thumb.png');
//        $image->flip()->save('./filp_image.png');
        $image->water('./logo.png',5,10)->save('water_image.png');
    }

    public function uploadImg(){
       /* print_r($_FILES);
        $file = $_FILES['file'];
        $tmp_name = $file['tmp_name'];
        $filename = "./1.jpg";
        move_uploaded_file($tmp_name,$filename);*/
        $file = request()->file('file');
        // 上传到本地服务器
//        $savename = \think\facade\Filesystem::putFile( 'topic', $file);
        $savename = \think\facade\Filesystem::disk('public')->putFile( 'goods', $file);
        echo $savename;
    }
    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
