<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\Request;
use think\facade\Db;
class Auth
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        // 1.查询所有一级分类 pid = 0
        $level1 = Db::name("auth")->where(["auth_pid"=>0])->select();

        // 2.查询所有的权限数据
        $levels = Db::name("auth")->order("auth_path")->select();
        $data = compact("level1","levels");
        return view("",$data);
    }

    // 权限添加处理
    public function addAuthAction(){
        $pid = request()->param("auth_pid");
        if( $pid == 0){
            // 添加的是一级分类
            $data = request()->only(["auth_name","auth_pid"]);
            $id = Db::name("auth")->insertGetId($data);
            if($id){
                $path = $id;
            }else{
                $path = false;
            }
        }else{
            // 添加的是二级分类
            $data = request()->only(["auth_name","auth_pid","auth_controller","auth_action"]);
            $id = Db::name("auth")->insertGetId($data);
            if($id){
                $path = Db::name("auth")->where(["id"=>$pid])->value("auth_path").",".$id;
            }else{
                $path = false;
            }
        }
        if(!$path) return json(["code"=>1]);
         if(Db::name("auth")->where(["id"=>$id])->update(["auth_path"=>$path])){
                $da['code'] = 0;
         }else{
                $da['code'] = 1;
         }
        return json($da);
    }
}
