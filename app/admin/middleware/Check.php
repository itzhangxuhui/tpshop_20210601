<?php
declare (strict_types = 1);

namespace app\admin\middleware;

use think\facade\Request;
use think\facade\Session;

class Check
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        return $next($request);

    }
    public function end(\think\Response $response)
    {
        // 回调行为
        $deny_arr = ["login","loginAction","loginOut"];
        if(!in_array(request()->action(),$deny_arr)){
            if(!Session::has("admin_flag")){
                // 跳转到登录
                return redirect((string)url('admin/Index/login'))->send();
            }
            // 判断权限 根据当前管理的id 查询管理员的权限 , 获取当前的控制器,方法,比对



        }


    }
}
