<?php
/*
    当前文件:Member.php    
    作者:Administrator       
    日期:2021/12/22         
*/
namespace app\admin\model;

use think\Model;
use think\model\concern\SoftDelete;
class MemberModel extends Model
{
    protected $name = 'member';
    use SoftDelete;
    protected $deleteTime = 'delete_time';

    protected $autoWriteTimestamp = 'datetime';

    protected $createTime = 'c';
    protected $updateTime = 'u';


}
