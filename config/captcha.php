<?php
// +----------------------------------------------------------------------
// | Captcha配置文件
// +----------------------------------------------------------------------

return [
    'length'    =>  3,
    'codeSet'   =>  '0123456789',
    'useNoise' => false,
];